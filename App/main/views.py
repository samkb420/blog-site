
from flask import render_template, request, Blueprint
from App.models import Post
import requests
import json

main = Blueprint('main', __name__)


@main.route("/")
@main.route("/home")
def home():
    r = requests.get('http://quotes.stormconsultancy.co.uk/random.json')
    page = request.args.get('page', 1, type=int)
    posts = Post.query.order_by(Post.date_posted.desc()).paginate(page=page, per_page=5)
    return render_template('home.html', posts=posts,qoutes=json.loads(r.text)['quote'],author=json.loads(r.text)['author'])


@main.route("/about")
def about():
    return render_template('about.html', title='About')