from tokenize import Comment
from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired


class PostForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    content = TextAreaField('Write blog here ...', validators=[DataRequired()])
    picture = FileField('Upload image here', validators=[FileAllowed(['jpg', 'png'])])
    submit = SubmitField('Post Blog')

class CommentForm(FlaskForm):
    comment = TextAreaField('Comment...', validators=[DataRequired()])
    submit = SubmitField('comment')